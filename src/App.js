import React from 'react';

import AdminLayout from './Components/Layout/AdminLayout/AdminLayout'
import Home from './Containers/Home/Home'
import Users from './Containers/Users/Users'
import Routers from './Containers/Routers/Routers'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import './App.css';
import {ApolloProvider} from '@apollo/react-hooks'
import ApolloClient from 'apollo-boost'

const apolloClient = new ApolloClient({uri: 'http://localhost:3044/graphql'})

function App() {
    return (
        <ApolloProvider client={apolloClient}>
            <Router>
                <div className="App">
                    <AdminLayout>
                        <Switch>
                            <Route path="/" exact component={Home}/>
                            <Route path="/users" component={Users}/>
                            <Route path="/routers" component={Routers}/>
                        </Switch>
                    </AdminLayout>
                </div>
            </Router>
        </ApolloProvider>

    );
}

export default App;
