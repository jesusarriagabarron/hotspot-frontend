import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import Counters from './../../Components/Counters/Counters'
import PosChart from './../../Components/Counters/PosChart'
import Grid from '@material-ui/core/Grid'
import ProfileChart from './../../Components/Counters/ProfilesChart'
import TableRouters from './../../Components/Counters/TableRouters'
import ActiveUsersChart from './../../Components/Counters/ActiveUsersChart'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    paperSpace: {
        // padding: theme.spacing(3, 2)
    }
}))

const color = '#4DC6DE';

const PosChartData = [
    ['Punto de Venta','Fichas Vendidas',{ role: 'style' }],
    ["FerreMateriales Hernandez",150,color],
    ["Miscelanea Don Pepe",120,color],
    ["Andres Manuel Lopez",80,color],
    ["Julio Profe",60,color],
    ["Merceria Anita",50,color]
];

const CountersData = {
    fichasActivas: 350,
    fichasVencidas: 893,
    ventasMes: 3405,
    fichasVendidasPromedio: 35
};

const ProfileChartData = [
    [ 'Perfiles', 'Vendidos'],
    ['Plan_1hora', 80],
    ['Plan_1dia', 26],
    ['Plan_1mes', 22],
    ['Plan_Vip', 10]
];

const TableRoutersData = [
    {name:'Ajalpan',calories:159, fat:6.0, carbs:24 },
    {name:'Altepexi', calories:237, fat:9.0,carbs: 37},
    {name:'Chilac', calories:262, fat:16.0, carbs:24},
    {name:'Tehuacan', calories:305, fat:3.7, carbs:67},
    {name:'Texcala', calories:356, fat:16.0, carbs:40}
];

const ActiveUsersChartData = {
    activeToday:80,
    data:[
        {
            name: 'Page A',
            uv: 4000,
            pv: 2400,
            amt: 2400
        }, {
            name: 'Page B',
            uv: 3000,
            pv: 1398,
            amt: 2210
        }, {
            name: 'Page C',
            uv: 2000,
            pv: 9800,
            amt: 2290
        }, {
            name: 'Page D',
            uv: 2780,
            pv: 3908,
            amt: 2000
        }, {
            name: 'Page E',
            uv: 1890,
            pv: 4800,
            amt: 2181
        }, {
            name: 'Page F',
            uv: 2390,
            pv: 3800,
            amt: 2500
        }, {
            name: 'Page G',
            uv: 3490,
            pv: 4300,
            amt: 2100
        }
    ]
};

const Home = (props) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Counters data={CountersData}/>
                <PosChart data={ PosChartData }/>
                <ProfileChart data={ProfileChartData}/>
                <TableRouters data={TableRoutersData}/>
                <ActiveUsersChart data={ActiveUsersChartData}/>
            </Grid>
        </div>

    );
}

export default Home;