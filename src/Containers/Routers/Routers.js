import React from 'react'
import Grid from '@material-ui/core/Grid'
import AddRouter from './../../Components/Routers/AddRouter'
import ListRouters from './../../Components/Routers/ListRouters'

const Routers = (props) => {

    return (
        <div >
            <Grid container spacing={2}>
                <AddRouter />
                <ListRouters/>
            </Grid>
        </div>

    )

}

export default Routers