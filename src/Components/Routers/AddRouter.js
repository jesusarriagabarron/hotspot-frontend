import React, {useState, useEffect} from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import styles from './AddRouter.module.css'
import {makeStyles} from '@material-ui/core/styles'
import RouterRounded from '@material-ui/icons/RouterRounded'
import DnsRoundedIcon from '@material-ui/icons/DnsRounded'
import TextField from '@material-ui/core/TextField'
import SaveIcon from '@material-ui/icons/SaveRounded';

import {useQuery} from '@apollo/react-hooks'
import {gql} from 'apollo-boost'
import {useMutation} from '@apollo/react-hooks'

const ADD_ROUTER = gql `
    mutation addRouter($input:RouterInput!){
        addRouter(input:$input){
            ip,
            name
        }
    }
`;

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: 200
        }
    },
    divButtons: {
        '& > *': {
            margin: 8
        }
    },
    buttonOddK: {
        background: 'linear-gradient(225deg, #4cb37a 19%, #60d031 90%)',
        borderRadius: 3,
        border: 0,
        color: 'white',
        height: 48,
        padding: '0 30px',
        boxShadow: '0 3px 5px 2px rgba(93, 93, 93, 0.3)'
    },
    textField: {
        margin: 8
    }
}));

const AddRouter = (props) => {
    const [routerInfo,
        setRouterInfo] = useState({ip: '', username: '', name: '', password: '', owner: '5e12e3a60979e8b37bd94e85'});
    const classes = useStyles();
    const [addRouter,{data}] = useMutation(ADD_ROUTER);

    const IconColor = '#77c0f5d4';

    return (

        <Grid item xs={12} sm={5}>
            <Paper>
                <Grid container direction="row" justify="flex-start" alignItems="center">
                    <RouterRounded
                        style={{
                        fontSize: '30px',
                        color: IconColor
                    }}/>
                    <Typography variant="body1">
                        Agregar Router
                    </Typography>
                </Grid>
                <Box mt={2} textAlign="center">
                    <Grid container>
                        <Grid item sm={12}>
                            <p>{routerInfo.ip}</p>
                            <p>{routerInfo.username}</p>

                            <TextField
                                label="Nombre del Router "
                                fullWidth
                                className={classes.textField}
                                value={routerInfo.name}
                                onChange={e => setRouterInfo({
                                ...routerInfo,
                                name: e.target.value
                            })}/>

                            <TextField
                                label="Dirección IP"
                                fullWidth
                                className={classes.textField}
                                value={routerInfo.ip}
                                onChange={e => setRouterInfo({
                                ...routerInfo,
                                ip: e.target.value
                            })}/>

                            <TextField
                                label="Usuario"
                                fullWidth
                                className={classes.textField}
                                value={routerInfo.username}
                                onChange={e => setRouterInfo({
                                ...routerInfo,
                                username: e.target.value
                            })}/>

                            <TextField
                                label="Password"
                                type="password"
                                fullWidth
                                className={classes.textField}
                                value={routerInfo.password}
                                onChange={e => setRouterInfo({
                                ...routerInfo,
                                password: e.target.value
                            })}/>

                            <Box mt={2} textAlign="center">
                                <div className={classes.divButtons}>
                                    <Button
                                        className="buttonOk"
                                        variant="contained"
                                        color="default"
                                        startIcon={< SaveIcon />}
                                        onClick = { e=>{ addRouter({variables:{input:routerInfo}})} }
                                        >
                                        Guardar
                                    </Button>
                                    <Button
                                        className="buttonWarning"
                                        variant="contained"
                                        color="secondary"
                                        startIcon={< DnsRoundedIcon />}>
                                        
                                        Probar conexion
                                    </Button>
                                </div>

                            </Box>
                        </Grid>
                    </Grid>
                </Box>
            </Paper>
        </Grid>

    )
}

export default AddRouter