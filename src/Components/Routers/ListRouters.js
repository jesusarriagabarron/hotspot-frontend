import React, {useState, useEffect} from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import styles from './ListRouters.module.css'
import {makeStyles} from '@material-ui/core/styles'
import RouterRounded from '@material-ui/icons/RouterRounded'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: 200
        }
    },
    divButtons: {
        '& > *': {
            margin: 8
        }
    },
    buttonOddK: {
        background: 'linear-gradient(225deg, #4cb37a 19%, #60d031 90%)',
        borderRadius: 3,
        border: 0,
        color: 'white',
        height: 48,
        padding: '0 30px',
        boxShadow: '0 3px 5px 2px rgba(93, 93, 93, 0.3)'
    },
    textField: {
        margin: 8
    }
}));

const ListRouters = (props) => {
    const [routerInfo,
        setRouterInfo] = useState({ipAddress: '', username: ''});
    const classes = useStyles();
    const IconColor = '#77c0f5d4';

    useEffect(() => {});

    return (

        <Grid item xs={12} sm={7}>
            <Paper>
                <Grid container direction="row" justify="flex-start" alignItems="center">
                    <RouterRounded
                        style={{
                        fontSize: '30px',
                        color: IconColor
                    }}/>
                    <Typography variant="body1">
                        Mis Routers
                    </Typography>
                </Grid>
                <Box mt={2} textAlign="center">
                    <Grid container>
                        <Grid item sm={12}>
                            <Table size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Router</TableCell>
                                        <TableCell align="right">Ip Address</TableCell>
                                        <TableCell align="right">Status</TableCell>
                                        <TableCell align="right">Usuarios activos </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Box>
            </Paper>
        </Grid>

    )
}

export default ListRouters