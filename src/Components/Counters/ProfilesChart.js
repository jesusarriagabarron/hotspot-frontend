import React from 'react'
import {Chart} from 'react-google-charts'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import Skeleton from '@material-ui/lab/Skeleton'
import NetworkCheckRoundedIcon from '@material-ui/icons/NetworkCheckRounded'

const ProfilesChart = (props) => {
    const IconColor = '#77c0f5d4';
    const loading = <Skeleton variant="rect" width={'100%'} height={200} />;

    return (
        <Grid item xs={12} sm={6}>
            <Paper>
                <Grid container direction="row" justify="flex-start" alignItems="center">
                    <NetworkCheckRoundedIcon
                        style={{
                        fontSize: '30px',
                        color: IconColor
                    }}/>
                    <Typography variant="body1">
                        Perfiles vendidos en POS - Diciembre
                    </Typography>
                </Grid>
                <Box mt={2}>
                <Grid container>
                    <Chart
                        width={'100%'}
                        height={'310px'}
                        chartType="PieChart"
                        loader={loading}
                        data={props.data}
                        options={{
                            pieHole: 0.4,
                            animation: {
                                startup: true,
                                easing: 'linear',
                                duration: 1500,
                              },
                    }}/>
                </Grid>
                </Box>
            </Paper>
        </Grid>
    )
}

export default ProfilesChart