import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import RouterRounded from '@material-ui/icons/RouterRounded'
import Typography from '@material-ui/core/Typography'

const TableRouters = (props) => {
    const IconColor = '#77c0f5d4';


    return (
        <Grid item xs={12} sm={8}>
            <Paper>
            <Grid container direction="row" justify="flex-start" alignItems="center">
                    <RouterRounded
                        style={{
                        fontSize: '30px',
                        color: IconColor
                    }}/>
                    <Typography variant="body1">
                        Routers
                    </Typography>
                </Grid>
                <Box mt={2}>
                <Grid container>
                <Table size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Router</TableCell>
                            <TableCell align="right">Fichas creadas</TableCell>
                            <TableCell align="right">Fichas activas</TableCell>
                            <TableCell align="right">Fichas expiradas</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.data.map(row => (
                            <TableRow key={row.name}>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell align="right">{row.calories}</TableCell>
                                <TableCell align="right">{row.fat}</TableCell>
                                <TableCell align="right">{row.carbs}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                </Grid>
                </Box>
            </Paper>
        </Grid>
    )

}

export default TableRouters;