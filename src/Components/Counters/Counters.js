import React from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import styles from './Counters.module.css'
import WifiIcon from '@material-ui/icons/WifiRounded'
import WifiOffIcon from '@material-ui/icons/WifiOffRounded'
import MonetizationOnRoundedIcon from '@material-ui/icons/MonetizationOnRounded'
import StorefrontRoundedIcon from '@material-ui/icons/StorefrontRounded'

const Counters = (props) => {
    return (
        <>
            <Grid item xs={12} sm={3}>
                <Paper className={styles.info1}>
                    <Box minHeight="80px" display="flex" alignItems="center">
                        <Grid container>
                            <Grid item sm={4}>
                                <WifiIcon
                                    style={{
                                    fontSize: '50px'
                                }}/>
                            </Grid>
                            <Grid item sm={8}>
                                <Typography variant="body1">
                                    Fichas Activas
                                </Typography>
                                <Typography variant="h5">
                                    <Box fontWeight="fontWeightBold">
                                        {props.data.fichasActivas}
                                    </Box>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
                <Paper className={styles.warning1}>
                    <Box minHeight="80px" display="flex" alignItems="center">

                        <Grid container>
                            <Grid item sm={4}>
                                <WifiOffIcon
                                    style={{
                                    fontSize: '50px'
                                }}/>
                            </Grid>
                            <Grid item sm={8}>
                                <Typography variant="body1">
                                    Fichas Expiradas
                                </Typography>
                                <Typography variant="h5">
                                    <Box fontWeight="fontWeightBold">
                                        {props.data.fichasVencidas}
                                    </Box>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
                <Paper className={styles.info2}>
                    <Box minHeight="80px" display="flex" alignItems="center">
                        <Grid container>
                            <Grid item sm={4}>
                                <MonetizationOnRoundedIcon
                                    style={{
                                    fontSize: '50px'
                                }}/>
                            </Grid>
                            <Grid item sm={8}>
                                <Typography variant="body1">
                                        Ventas Diciembre 19
                                </Typography>
                                <Typography variant="h5">
                                    <Box fontWeight="fontWeightBold">
                                        ${props.data.ventasMes}
                                    </Box>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
                <Paper className={styles.info3}>
                    <Box minHeight="80px" display="flex" alignItems="center">
                        <Grid container>
                            <Grid item sm={4}>
                                <StorefrontRoundedIcon
                                    style={{
                                    fontSize: '50px'
                                }}/>
                            </Grid>
                            <Grid item sm={8}>
                                <Typography variant="body2">
                                    Promedio Fichas Vendidas por POS
                                </Typography>
                                <Typography variant="h5">
                                    <Box fontWeight="fontWeightBold">
                                        {props.data.fichasVendidasPromedio}
                                    </Box>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Paper>
            </Grid>
        </>
    )
}


export default Counters