import React from 'react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import Skeleton from '@material-ui/lab/Skeleton'
import BarChartRoundedIcon from '@material-ui/icons/BarChartRounded'
import {Chart} from 'react-google-charts'

const PosChart = (props) => {

    const IconColor = '#77c0f5d4';
    const loading = <Skeleton variant="rect" width={'100%'} height={200} />;

    return (
        <Grid item xs={12} sm={6}>
            <Paper>
                <Grid container direction="row" justify="flex-start" alignItems="center">
                    <BarChartRoundedIcon
                        style={{
                        fontSize: '40px',
                        color:IconColor
                    }}/>
                    <Typography variant="body1">
                        Fichas Generadas y Entregadas - Top 5
                    </Typography>
                </Grid>
                <Box mt={2}>
                <Grid container>
                    <Chart
                        width={'100%'}
                        height={'300px'}
                        chartType="Bar"
                        loader={loading}
                        data={props.data}
                        options={{
                        hAxis: {
                            title: 'Fichas Expedidas',
                        },
                        vAxis: {
                            title: 'Punto de Venta'
                        },
                        bars: 'horizontal',
                        axes: {
                          y: {
                            0: { side: 'right' },
                          },
                        },
                        animation: {
                            startup: true,
                            easing: 'linear',
                            duration: 1000,
                          },
                          legend: { position: 'none' }
                    }}/>
                </Grid>
                </Box>
                
            </Paper>
        </Grid>
    )

}

export default PosChart;