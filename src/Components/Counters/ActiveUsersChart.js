import React from 'react'
import {AreaChart, Area, ResponsiveContainer} from 'recharts'
import {Grid,Paper,Box,Typography} from '@material-ui/core'
import RouterRounded from '@material-ui/icons/RouterRounded'


const ActiveUsersChart = (props) => {

    const IconColor = '#77c0f5d4';

    
    return (
        <Grid item xs={12} sm={4}>
            <Paper>
                <Grid container direction="row" justify="flex-start" alignItems="center">
                    <RouterRounded
                        style={{
                        fontSize: '30px',
                        color: IconColor
                    }}/>
                    <Typography variant="body1">
                        Usuarios Activos
                    </Typography>
                </Grid>
                <Box mt={2} textAlign="center">
                    <Typography variant="h4">
                        {props.data.activeToday} 
                    </Typography>
                    <Typography variant="body2">
                        Usuarios Activos Hoy
                    </Typography>
                    <Grid container>
                        <div style={{width:'100%',height:140}}>
                        <ResponsiveContainer>
                        <AreaChart
                            data={props.data.data}
                            margin={{
                                top: 0, right: 0, left: 0, bottom: 0,
                              }}>
                            <Area type='monotone' dataKey='uv' stroke='#8884d8' fill='#8884d8'/>
                        </AreaChart>
                        </ResponsiveContainer>
                        </div>
                    </Grid>
                </Box>
            </Paper>
        </Grid>
    )

}

export default ActiveUsersChart;