import React from 'react'
import AppMainBar from '../../AppMainBar/AppMainBar'

const AdminLayout = (props) => {

    return (
        <AppMainBar>
            <main>
                {props.children}
            </main>
        </AppMainBar>
    );

}

export default AdminLayout;