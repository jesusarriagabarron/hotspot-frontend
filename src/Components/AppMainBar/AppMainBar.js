import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import ToolBar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Drawer from '@material-ui/core/Drawer'
import Divider from '@material-ui/core/Divider'
import Hidden from '@material-ui/core/Hidden'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import {makeStyles} from '@material-ui/core/styles'
import RouterIcon from '@material-ui/icons/Router'
import HomeIcon from '@material-ui/icons/HomeRounded'
import PeopleIcon from '@material-ui/icons/PeopleAltRounded'
import NetworkCheckRoundedIcon from '@material-ui/icons/NetworkCheckRounded'
import StorefrontRoundedIcon from '@material-ui/icons/StorefrontRounded'
import {Link} from 'react-router-dom'

//set drawer size
const drawerWidth = 250;

//styling Material UI
const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex'
    },
    rightToolbar: {
        marginLeft: 'auto',
        marginRight: -12
    },
    drawer: {
        [
            theme
                .breakpoints
                .up('lg')
        ]: {
            width: drawerWidth,
            flexShrink: 0
        }
    },
    appBar: {
        [
            theme
                .breakpoints
                .up('lg')
        ]: {
            width: `calc(100% - ${drawerWidth}px)`
        },
        background: 'linear-gradient(350deg, #FE6B8B 30%, #FF8E53 90%)',
        color: '#fff'
    },
    mainContent: {
        width: '100%',
        padding: '0 15px',
        [
            theme
                .breakpoints
                .up('lg')
        ]: {
            width: `calc(100% - ${drawerWidth}px)`,
            padding: '0 15px'
        }
    },
    paper: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        width: drawerWidth,
        border: 0
    },
    content: {
        flexGrow: 1
    },
    icon: {
        color: '#fff'
    },
    logo: {
        padding: '30px 10px',
        textAlign: 'center',
        background: '#fff'
    },
    logoimg: {
        width: '180px',
        height: 'auto'
    }
}));

//React component
const AppMainBar = (props) => {
    const classes = useStyles();

    //Sidebar
    const drawerSidebar = (
        <nav className={classes.drawer}>
            <Drawer
                classes={{
                paper: classes.paper
            }}
                variant="permanent"
                open>
                <div className={classes.logo}>
                    <img
                        className={classes.logoimg}
                        src="http://2red.mx/wp-content/uploads/2019/03/logo2REDrecortado.png"
                        alt="2red"></img>
                </div>
                <Divider/>
                <Link to="/">
                    <ListItem button>
                        <ListItemIcon
                            classes={{
                            root: classes.icon
                        }}>
                            <HomeIcon/>
                        </ListItemIcon>
                        <ListItemText className={classes.icon} primary="Dashboard"/>
                    </ListItem>
                </Link>

                <Link to="/routers">
                    <ListItem button>
                        <ListItemIcon
                            classes={{
                            root: classes.icon
                        }}>
                            <RouterIcon/>
                        </ListItemIcon>
                        <ListItemText className={classes.icon} primary="Routers"/>
                    </ListItem>
                </Link>

                <ListItem button>
                    <ListItemIcon
                        classes={{
                        root: classes.icon
                    }}>
                        <PeopleIcon/>
                    </ListItemIcon>
                    <ListItemText className={classes.icon} primary="Fichas"/>
                </ListItem>
                <ListItem button>
                    <ListItemIcon
                        classes={{
                        root: classes.icon
                    }}>
                        <NetworkCheckRoundedIcon/>
                    </ListItemIcon>
                    <ListItemText className={classes.icon} primary="Perfiles"/>
                </ListItem>
                <ListItem button>
                    <ListItemIcon
                        classes={{
                        root: classes.icon
                    }}>
                        <StorefrontRoundedIcon/>
                    </ListItemIcon>
                    <ListItemText className={classes.icon} primary="Puntos de Venta"/>
                </ListItem>

            </Drawer>
        </nav>
    );

    //Render Main App
    return (
        <div className={classes.root}>
            <AppBar className={classes.appBar} position="fixed">
                <ToolBar>
                    <Hidden smUp>
                        <IconButton edge="start" color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                    </Hidden>
                    <Typography variant="h6">
                        mikroticket!
                    </Typography>
                    <section className={classes.rightToolbar}>
                        <IconButton edge="end" color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                        <Button color="inherit">Login</Button>
                    </section>
                </ToolBar>
            </AppBar>

            {drawerSidebar}

            <div className={"mainContent " + classes.mainContent}>
                {props.children}
            </div>
        </div>
    )
}

export default AppMainBar;